/**
 *
 * @param options
 */
var storeCreator = function(options){

    /*
     * Variables accessible
     * in the class
     */
    var vars = {
        myVar  : 'original Value'
    };

    /*
     * Can access this.method
     * inside other methods using
     * root.method()
     */
    var root = this;

    /*
     * Constructor
     */
    this.construct = function(options){
        $.extend(vars , options);
    };

    /*
     * Public method
     * Can be called outside class
     */
    this.init = function(){
        this.initCheckboxes();
        this.showStore();
    };

    /**
     *
     */
    this.initCheckboxes = function() {
        $('.block_container').hide();
        var checkboxes = $('.visibility_check');

        var block;
        var checked;
        $.each(checkboxes, function(i,el){
            block = $(el).data('block');
            checked = $(el).prop('checked');
            if(checked){
                $('#'+block).show();
            }
        })
    };

    this.addStore = function () {
        var oldItems = JSON.parse(localStorage.getItem('storeItemsArray')) || [];
        var formData = $('#store_gen_form').serializeArray();
        var newItem = {};
        var exit = false;
        $(formData).each(function (i, v) {
            var nam = v.name;
            var val = v.value;
            if(nam != 'root_category' && val == ''){
                exit = true;
            }
            newItem[nam] = val;
        });
        if(exit){
            return false;
        }
        var repeated=oldItems.filter(function(a){ return a==JSON.stringify(newItem)}).length;
        if(!repeated){
            oldItems.push(newItem);
            localStorage.setItem('storeItemsArray', JSON.stringify(oldItems));
        }else{alert('already added')}
        this.showStore();
        $('#store_gen_form').trigger("reset");
    };

    this.showStore = function () {
        var oldItems = JSON.parse(localStorage.getItem('storeItemsArray')) || [];

        var out = '';
        $(oldItems).each(function (i, v) {
            out += "[<br/>\
                                        &nbsp;&nbsp;&nbsp;'website_code' => '"+ v.website_code + "',<br/>\
                        &nbsp;&nbsp;&nbsp;'website_name' => '"+ v.website_name + "',<br/>\
                        &nbsp;&nbsp;&nbsp;'group_code' => '"+ v.store_code + "',<br/>\
                        &nbsp;&nbsp;&nbsp;'group_name' => '"+ v.store_name + "',<br/>\
                        &nbsp;&nbsp;&nbsp;'root_category' => '"+ v.root_category + "',<br/>\
                        &nbsp;&nbsp;&nbsp;'store_code' => '"+ v.store_view_code + "',<br/>\
                        &nbsp;&nbsp;&nbsp;'store_name' => '"+ v.store_view_name + "',<br/>\
                        &nbsp;&nbsp;&nbsp;'is_active' => '1'<br/>\
                ],"
        });
        out = out.substring(0, out.length - 1);
        $('#store_gen_result').html(out);
    };

    this.resetStore = function () {
        localStorage.setItem('storeItemsArray', JSON.stringify([]));
        this.showStore();
    };


    /*
     * Pass options when class instantiated
     */
    this.construct(options);

};