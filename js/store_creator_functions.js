var storeCreatorIstance = new storeCreator();

$(document).ready(function() {
    storeCreatorIstance.init();


    $('#store_gen_form').submit(function (e) {
        e.preventDefault();

        var formData = $('#store_gen_form').serializeArray();
        var arr = [];
        $(formData).each(function (i, v) {
            arr[v.name] = v.value;
        });
        var storeArray = "[<br/>\
                        &nbsp;&nbsp;&nbsp;'website_code' => '"+ arr.website_code + "',<br/>\
                        &nbsp;&nbsp;&nbsp;'website_name' => '"+ arr.website_name + "',<br/>\
                        &nbsp;&nbsp;&nbsp;'group_code' => '"+ arr.store_code + "',<br/>\
                        &nbsp;&nbsp;&nbsp;'group_name' => '"+ arr.store_name + "',<br/>\
                        &nbsp;&nbsp;&nbsp;'root_category' => '"+ arr.root_category + "',<br/>\
                        &nbsp;&nbsp;&nbsp;'store_code' => '"+ arr.store_view_code + "',<br/>\
                        &nbsp;&nbsp;&nbsp;'store_name' => '"+ arr.store_view_name + "',<br/>\
                        &nbsp;&nbsp;&nbsp;'is_active' => '1'<br/>\
                    ]";

        $('#store_gen_result').html(storeArray);

        console.log(arr);
    });
    $('#locale_gen_form').submit(function (e) {
        e.preventDefault();

        var formData = $('#locale_gen_form').serializeArray();
        var arr = [];
        $(formData).each(function (i, v) {
            arr[v.name] = v.value;
        });
        var localeArray;
        if (arr.scope_locale_code === 'locale_code') {
            localeArray = "[<br/>\
                        &nbsp;&nbsp;&nbsp;'name' => '"+ arr.scope_locale_code + "',<br/>\
                        &nbsp;&nbsp;&nbsp;'value' => '"+ arr.locale_code + "'<br/>\
                    ]";
        }
        else {
            localeArray = "[<br/>\
                        &nbsp;&nbsp;&nbsp;'name' => '"+ arr.scope_locale_code + "',<br/>\
                        &nbsp;&nbsp;&nbsp;'ref' => '"+ arr.ref_name + "',<br/>\
                        &nbsp;&nbsp;&nbsp;'value' => '"+ arr.locale_code + "'<br/>\
                    ]";
        }


        $('#locale_gen_result').html(localeArray);

        console.log(arr);
    });
    $('#default_country_gen_form').submit(function (e) {
        e.preventDefault();

        var formData = $('#default_country_gen_form').serializeArray();
        var arr = [];
        $(formData).each(function (i, v) {
            arr[v.name] = v.value;
        });
        var localeArray;
        if (arr.scope_default_country === 'default_country') {
            localeArray = "[<br/>\
                        &nbsp;&nbsp;&nbsp;'name' => '"+ arr.scope_default_country + "',<br/>\
                        &nbsp;&nbsp;&nbsp;'value' => '"+ arr.default_country + "'<br/>\
                    ]";
        }
        else {
            localeArray = "[<br/>\
                        &nbsp;&nbsp;&nbsp;'name' => '"+ arr.scope_default_country + "',<br/>\
                        &nbsp;&nbsp;&nbsp;'ref' => '"+ arr.ref_name + "',<br/>\
                        &nbsp;&nbsp;&nbsp;'value' => '"+ arr.default_country + "'<br/>\
                    ]";
        }


        $('#default_country_gen_result').html(localeArray);

        console.log(arr);
    });
    $('#allow_country_gen_form').submit(function (e) {
        e.preventDefault();

        var formData = $('#allow_country_gen_form').serializeArray();
        var arr = [];
        $(formData).each(function (i, v) {
            arr[v.name] = v.value;
        });
        var localeArray;
        if (arr.scope_allow_country === 'allow_country') {
            localeArray = "[<br/>\
                        &nbsp;&nbsp;&nbsp;'name' => '"+ arr.scope_allow_country + "',<br/>\
                        &nbsp;&nbsp;&nbsp;'value' => '"+ $('#allow_country').val() + "'<br/>\
                    ]";
        }
        else {
            localeArray = "[<br/>\
                        &nbsp;&nbsp;&nbsp;'name' => '"+ arr.scope_allow_country + "',<br/>\
                        &nbsp;&nbsp;&nbsp;'ref' => '"+ arr.ref_name + "',<br/>\
                        &nbsp;&nbsp;&nbsp;'value' => '"+ $('#allow_country').val() + "'<br/>\
                    ]";
        }


        $('#allow_country_gen_result').html(localeArray);

        console.log(arr);
    });
    $('#currency_base_gen_form').submit(function (e) {
        e.preventDefault();

        var formData = $('#currency_base_gen_form').serializeArray();
        var arr = [];
        $(formData).each(function (i, v) {
            arr[v.name] = v.value;
        });
        var localeArray;
        if (arr.scope_currency_base === 'currency_base') {
            localeArray = "[<br/>\
                        &nbsp;&nbsp;&nbsp;'name' => '"+ arr.scope_currency_base + "',<br/>\
                        &nbsp;&nbsp;&nbsp;'value' => '"+ $('#currency_base').val() + "'<br/>\
                    ]";
        }
        else {
            localeArray = "[<br/>\
                        &nbsp;&nbsp;&nbsp;'name' => '"+ arr.scope_currency_base + "',<br/>\
                        &nbsp;&nbsp;&nbsp;'ref' => '"+ arr.ref_name + "',<br/>\
                        &nbsp;&nbsp;&nbsp;'value' => '"+ $('#currency_base').val() + "'<br/>\
                    ]";
        }


        $('#currency_base_gen_result').html(localeArray);

        console.log(arr);
    });
    $('#currency_default_gen_form').submit(function (e) {
        e.preventDefault();

        var formData = $('#currency_default_gen_form').serializeArray();
        var arr = [];
        $(formData).each(function (i, v) {
            arr[v.name] = v.value;
        });
        var localeArray;
        if (arr.scope_currency_default === 'currency_default') {
            localeArray = "[<br/>\
                        &nbsp;&nbsp;&nbsp;'name' => '"+ arr.scope_currency_default + "',<br/>\
                        &nbsp;&nbsp;&nbsp;'value' => '"+ $('#currency_default').val() + "'<br/>\
                    ]";
        }
        else {
            localeArray = "[<br/>\
                        &nbsp;&nbsp;&nbsp;'name' => '"+ arr.scope_currency_default + "',<br/>\
                        &nbsp;&nbsp;&nbsp;'ref' => '"+ arr.ref_name + "',<br/>\
                        &nbsp;&nbsp;&nbsp;'value' => '"+ $('#currency_default').val() + "'<br/>\
                    ]";
        }


        $('#currency_default_gen_result').html(localeArray);

        console.log(arr);
    });
});

$(document).on('click', '.visibility_check', function (){
    storeCreatorIstance.initCheckboxes();
});
$(document).on('click','.add_store', function(){
    storeCreatorIstance.addStore();
});
$(document).on('click','.reset_store', function(){
    storeCreatorIstance.resetStore();
});
$(document).on('click','.show_store', function(){
    storeCreatorIstance.showStore();
});

(function () {
    'use strict';

    window.addEventListener('load', function () {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');

        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function (form) {
            form.addEventListener('submit', function (event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
})();